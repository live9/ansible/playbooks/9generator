# 9generator - Docker projects bootstrapping

This repository contains Ansible plabooks to render configuration files for different types of projects that are going to be deployed using a CI/CD platform.

## Project Description

Functionality is organized by roles. There is a [main role](https://gitlab.com/live9/ansible/roles/9generator-project) that handles all the common tasks, like template rendering, copying files or file permissions adjustments. Then there is a role per project type that uses the main role and adds all the templates and other files needed, also with any custom tasks needed to be executed for that particular project type. So far these are the roles that have been developed:

* [_9generator-core_](https://gitlab.com/live9/ansible/roles/9generator-core): Main role that does all the bootstrapping for a project.
* [_9generator-jekyll_](https://gitlab.com/live9/ansible/roles/9generator-jekyll): Role to bootstrap jekyll projects.
* [_9generator-terraform_](https://gitlab.com/live9/ansible/roles/9generator-terraform): Role to bootstrap terraform projects, currently it only generates the `terraform` block with the remote state s3 bucket it needs to use.
* [_9generator-portainer_](https://gitlab.com/live9/ansible/roles/9generator-portainer): Role to bootstrap docker swarm files for [portainer](https://portainer.io) services.
* [_9generator-traefik_](https://gitlab.com/live9/ansible/roles/9generator-traefik): Role to bootstrap docker swarm files for [traefik](https://traefik.io) services.

## Usage instructions

Bellow are the instructions on how to use this project. First it needs to be configured for a project, then run to bootstrap it.

### Configuration of a Project

The templates use [jinja2](https://jinja2docs.readthedocs.io/en/stable/) format. Those templates can, for example, create customized [docker-compose](https://docs.docker.com/compose/) and [docker stack](https://docs.docker.com/engine/swarm/stack-deploy/) files. Each project that uses one of these roles (ie, jekyll, wordpress or terraform) need to have a `manifest.yml` file (in YAML format) with all the variables needed to render the templates of that role.

This is an [example manifest file](manifest.yml.example). The following variables are needed to run the playbook:

* _project_type_: The set of templates to use, the playbook will look for a directory named as defined with this variable inside the _templates_ directory (ie: project_type: _jekyll_ will use the role _jekyll_). This parameter is _mandatory_ and the playbook will not work without this.
* _file_owner_id_: User ID to set on the rendered files. By default _root_ user will be the owner (ej: file_owner_id: 1000 will set ownership of files to id 1000).
* _out_dir_: Output directory where to store rendered files in the application project directory. By default files will be put on ../, if set then ../{{ out_dir }} (This variable is optional, files will be copied to ../ by default).
* _manifest_path_: Path to the project's manifest file. This parameter is _mandatory_ and the playbook will not work without this.

The rest of variables are dependent for the templates of each role (and can be mandatory for _those_ templates).

### Execution

An Ansible playbook uses these roles and the project's `manifest.yml` file as input, and as output it produces rendered versions of the templates with the variable values from the `manifest.yml` replaced on them and copies other files to the project application directory.

**_NOTE:_** Currently to use this project it needs to be added as a [_git submodule_](https://git-scm.com/book/en/v2/Git-Tools-Submodules) inside each website that needs to generate these files so it can find the variables file on `../` path.


For example, to render the files for a project, cd into the project (which already contains `9generator` as a _submodule_) and run the playbook:

```
ansible-playbook 9generator/playbook.yml --extra-vars "manifest_path=../manifest.yml"
```

If using an ansible docker container, the directory to the application project must be mounted as a bind volume to the ansible docker container. So inside the project's directory as before run the container as this:

```
docker run -ti --rm -v $PWD:/project --workdir /project live9/ansible playbook.yml --extra-vars "manifest_path=../manifest.yml"

```

That command will run the playbook locally as this tool is designed to be run as part of a CI/CD pipeline so it will run on the same machine where the application deployment is being done. If it needs to be used remotely then add to the `ansible-playbook` the need parameters, like the inventory of any other variable with `--extra-vars`.
